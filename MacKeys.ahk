#InstallKeybdHook
#SingleInstance force
SetTitleMatchMode 2
SendMode Input

; https://autohotkey.com/docs/Hotkeys.htm
; # = win key
; ! = alt key
; ^ = ctrl key
; + = shift key
; &	An ampersand may be used between any two keys or mouse buttons to combine them into a custom hotkey.

; switch between header+footer in Visual Studio using the OSX shortcut cmd-control-up
^#Up::Send {ctrl down}{k}{o}{ctrl up})

; Navigation of smaller chunks (skip word)
<!Left::Send {ctrl down}{Left}{ctrl up}
<!Right::Send {ctrl down}{Right}{ctrl up}

; Navigation using of bigger chunks (Skip to start/end of line/paragraph/document)
^Left::Send {Home}
^Right::Send {End}
!Up::Send {ctrl down}{Up}{ctrl up}
!Down::Send {ctrl down}{Down}{ctrl up}
^Up::Send {Lctrl down}{Home}{Lctrl up}
^Down::Send {Lctrl down}{End}{Lctrl up}

; Selection (uses a combination of the above with shift)
<!+Left::Send {ctrl down}{shift down}{Left}{shift up}{ctrl up}
<!+Right::Send {ctrl down}{shift down}{Right}{shift up}{ctrl up}
^+Left::Send {shift down}{Home}}{shift up}
^+Right::Send {shift down}{End}}{shift up}
!+Up::Send {ctrl down}{shift down}{Up}}{shift up}{ctrl up}
!+Down::Send {ctrl down}{shift down}{Down}}{shift up}{ctrl up}
^+Up::Send {Lctrl down}{shift down}{Home}}{shift up}{Lctrl up}
^+Down::Send {Lctrl down}{shift down}{End}}{shift up}{Lctrl up}



; temp until windows key remapped right?
<#Left::Send {Home}
<#Right::Send {End}
<#Up::Send {Lctrl down}{Home}{Lctrl up}
<#Down::Send {Lctrl down}{End}{Lctrl up}

<#+Left::Send {shift down}{Home}}{shift up}
<#+Right::Send {shift down}{End}}{shift up}
<#+Up::Send {Lctrl down}{shift down}{Home}}{shift up}{Lctrl up}
<#+Down::Send {Lctrl down}{shift down}{End}}{shift up}{Lctrl up}

;The following section replaces Ctrl+Key with  Win+Key
#a::^a
#b::^b
#c::^c
#d::^d 
#e::^e
;following won't remap using the normal method
#f::Send {LCtrl down}{f}{LCtrl up}
<#g::Send {F3}
<#+g::Send {shift down}{F3}{shift up}
;^g
#h::^h
#i::^i
#j::^j
#k::^k
#l::^g ;Send {LCtrl down}{l}{LCtrl up} ;disabled, I like winkey-L
#m::^m
#n::^n
#o::^o
#p::^p
;#q::^q ;disabled --remapped to alt-F4 instead
#r::^r
#s::^s
#t::^t
#u::^u
#v::^v
;#w::^w ;disabled --remapped to ctrl-F4 instead
#x::^x
#y::^y
#z::^z
#1::^1
#2::^2
#3::^3
#4::^4
#5::^5
#6::^6
#7::^7
#8::^8
#9::^9
#0::^0

#=::^=
#-::^-
